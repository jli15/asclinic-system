.. _git:

GIT - VERSIONING CONTROL FOR CODE
=================================

.. toctree::
  :maxdepth: 2
  :hidden:

  git_what_and_why
  git_workings



.. admonition:: Contents of this part posed as questions

  #. :ref:`git-what-and-why` Explain at a high-level.
  #. Explain the key frameworks and workflows for :ref:`git-workings` to collaboratively develop code as a team.